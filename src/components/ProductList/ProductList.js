import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import '../ProductList/ProductList.scss'

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: this.props.active
        };

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }


    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({
                active: false,
                buttonActive: true
            });
            document.querySelector('.App').classList.toggle('active');
        }
    }

    render() {
        const products = this.props.productList.map((item, num) => <ProductCard key={num} onClickAlert={this.props.onAdd} onStarClick={this.props.onStar} userInfo={item} products={this.props.productList} onStarChangeProduct={this.props.onStarChangeApp}/>);
        return (
            <>
                <div className="container">
                    <div className={"productList"}>
                        {products}
                    </div>
                </div>
            </>
        );
    }
}

export default ProductList;