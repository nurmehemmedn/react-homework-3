import React, {Component} from 'react';
import './Cart.scss';
import CartProductCard from '../Cart/CartProductCard/CartProductCard'

class Cart extends Component {
    constructor(props) {
        super(props);
        localStorage.setItem('Carts',JSON.stringify(this.props.productList));
        this.state = {
            productList: JSON.parse(localStorage.getItem('Carts'))
        }
    }


    render() {
        let products;
        if (this.state.productList !== null && this.state.productList !== undefined) {
            products = this.state.productList.map((item, num) => <CartProductCard key={num}
                                                                              onClickAlert={this.props.onclick}
                                                                              userInfo={item}/>);
        } else {
            products = []
        }
        return (
            <>
                <div className="container">
                    <h1>My Cart Products</h1>
                    <div className={"productList"}>
                        {products}
                    </div>
                </div>
            </>
        );
    }
}

export default Cart;