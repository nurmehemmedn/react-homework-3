import React, {Component} from 'react';
import FavoriteProductCard from "./FavoriteProductCard/FavoriteProductCard";

class Favorites extends Component {
    constructor(props) {
        super(props);
        localStorage.setItem('favorites',JSON.stringify(this.props.productLists));
        this.state = {
            productLists: JSON.parse(localStorage.getItem('favorites'))
        }
    }


    render() {
        let products;
        if (this.state.productLists !== null && this.state.productLists !== undefined) {
            products = this.state.productLists.map((item, ind) => <FavoriteProductCard key={ind}
                                                                                  onClickAlert={this.props.onclick}
                                                                                  userInfo={item}/>);
        } else {
            products = []
        }
        return (
            <>
                <div className="container">
                    <h1>My Favorite Products</h1>
                    <div className={"productList"}>
                        {products}
                    </div>
                </div>
            </>
        );
    }
}

export default Favorites;