import React, {Component} from 'react';
import './ProductCard.scss';
import StarRatingComponent from 'react-star-rating-component';

class ProductCard extends Component {
    constructor(props) {
        super(props);
        if (JSON.parse(localStorage.getItem('favorites')) !== null || JSON.parse(localStorage.getItem('favorites')) !== undefined) {
        this.state = {
            rating: 0,
            products:this.props.products,
            favorites:JSON.parse(localStorage.getItem('favorites'))
        }
        } else {
            this.state = {
                rating: 0,
                products:this.props.products,
                favorites:[]
            }
        }
    }

    render() {
        console.log(this.props.userInfo.productNumber);
        const { rating } = this.state;
        return (
                <div id={this.props.userInfo.productNumber} key={this.props.userInfo.productNumber} className="card">
                    <img className={"Product-Image"} src={this.props.userInfo.imgSrc} alt="Denim Jeans"/>
                    <p className="Product-Name">{this.props.userInfo.name}</p>
                        <p className="price">${this.props.userInfo.price}</p>
                    <h2>Rating from state: {rating}</h2>
                    <div onClick={this.props.onStarChangeProduct}>
                    <StarRatingComponent
                        name="Rate"
                        starCount={5}
                        value={rating}
                        onStarClick={this.props.onStarClick.bind(this)}
                    />
                    </div>
                        <p><button onClick={this.props.onClickAlert}>Add to Cart</button></p>
                </div>
              )
    }
}
export default ProductCard;