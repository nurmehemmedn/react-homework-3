import React, {Component} from 'react';
import '../../components/Button/Button.scss'
import PropTypes from 'prop-types'

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundColor : this.props.backgroundColor,
            text : this.props.text,
            onClick : this.props.onclick
        }
    }
    render() {
        return (
            <button className="buttonStyle" style={{backgroundColor: this.state.backgroundColor}} onClick={this.state.onClick}>{this.state.text}</button>
        );
    }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;