import React, { Component} from 'react';
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

class Alert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstModal: this.props.firstModalActive,
            buttonActive: true
        };
    }

    render() {
        return (
            this.state.firstModal ?
        <div className="App">
            <div ref={this.setWrapperRef} className="modalContainer">
                <Modal header="Do you want to add this product?" closeButton={true} text="
            Are you sure you want to add it?" closeSpan={this.props.closeSpan}
                       actions={[<Button key="1" backgroundColor="#b3382c" text="Ok" onclick={this.props.okFunction}/>,
                           <Button key="2" backgroundColor="#b3382c" text="Cancel" onclick={this.props.cancelFunction}/>]}
                /></div>
        </div> : null

        );
    }
}
export default Alert;
